package homelab_dns

import (
	"errors"
	"fmt"
	"net"
	"testing"
)

func ExampleResolver_Resolve() {
	// Setup a LookupIP function which returns a constant IP
	returnValue := []net.IP{net.ParseIP("100.100.100.100")}
	spyLookup := &SpyLookup{ReturnValue: returnValue}
	resolver := NewResolver(spyLookup.LookupIP)

	result, err := resolver.Resolve("example.com")

	if err != nil {
		fmt.Println("failed to resolve hostname")
	}

	fmt.Println(result)
	// Output: 100.100.100.100
}

func TestResolveNameToIP(t *testing.T) {
	t.Run("resolver returns ip of host", func(t *testing.T) {
		wantIP := "100.100.100.100"
		returnValue := []net.IP{net.ParseIP(wantIP)}
		spyLookup := &SpyLookup{ReturnValue: returnValue}
		resolver := NewResolver(spyLookup.LookupIP)
		result, _ := resolver.Resolve("straessle.me")

		if len(spyLookup.LookUps) != 1 {
			t.Fatalf("Expected to call LookupIP")
		}

		want := "straessle.me"
		got := spyLookup.LookUps[0]

		if want != got {
			t.Errorf("Expected %q to be %q", got, want)
		}

		if result != wantIP {
			t.Errorf("Expected %q to be %q", result, wantIP)
		}
	})
	t.Run("resolver returns error if lookup fails", func(t *testing.T) {
		var returnValue []net.IP
		spyLookup := &SpyLookup{ReturnValue: returnValue, ReturnError: errors.New("Lookup failed")}
		resolver := NewResolver(spyLookup.LookupIP)
		_, err := resolver.Resolve("straessle.me")

		if err == nil {
			t.Errorf("Expected resolver to return error but got none")
		}
	})
}
