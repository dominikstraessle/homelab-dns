package main

import (
	"github.com/cloudflare/cloudflare-go"
	_ "github.com/spf13/cobra"
	homelabdns "homelab-dns"
	"log"
	"net"
	"net/http"
	"os"
)

func main() {
	resolver := *homelabdns.NewResolver(net.LookupIP)

	api, err := cloudflare.NewWithAPIToken(os.Getenv("CF_API_KEY"))
	if err != nil {
		log.Fatal(err)
	}
	zoneID := os.Getenv("CF_ZONE_ID")
	client := *homelabdns.NewClient(api, zoneID)
	referenceUrl := os.Getenv("REFERENCE_URL")
	app := homelabdns.NewDefaultApp(resolver, client)
	server := homelabdns.NewAppServer(app, referenceUrl)

	if err := http.ListenAndServe(":8080", server); err != nil {
		log.Fatalf("could not listen on port 5000 %v", err)
	}
}
