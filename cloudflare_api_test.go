package homelab_dns

import (
	"context"
	"errors"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"reflect"
	"testing"
	"time"
)

func ExampleClient_GetARecords() {
	// Setup Spy to simulate cloudflare
	spyApi := &SpyApi{
		ReturnValue: []cloudflare.DNSRecord{
			{Name: "straessle.me"},
		},
	}
	zoneID := "123"

	client := NewClient(spyApi, zoneID)
	records, err := client.GetARecords()

	if err != nil {
		fmt.Println("failed to get A records")
		return
	}

	name := records[0].Name

	fmt.Println(name)
	// Output: straessle.me
}

func ExampleClient_UpdateRecordContent() {
	// Setup Spy to simulate cloudflare
	spyApi := &SpyApi{}
	zoneID := "123"
	ctx := context.Background()

	client := NewClient(spyApi, zoneID)
	oldRecord := cloudflare.DNSRecord{
		ID:      "1",
		Name:    "straessle.me",
		Content: "0.0.0.0",
	}
	newContent := "1.1.1.1"

	err := client.UpdateRecordContent(ctx, oldRecord, newContent)

	if err != nil {
		fmt.Println("failed to update records")
		return
	}

	// Output:
}

func TestGetDNSARecordsOfHost(t *testing.T) {
	t.Run("get dns records of host of type A", func(t *testing.T) {
		spyApi := &SpyApi{
			ReturnValue: []cloudflare.DNSRecord{
				{Name: "straessle.me"},
			},
		}
		zoneID := "123"
		client := NewClient(spyApi, zoneID)

		result, _ := client.GetARecords()

		if len(spyApi.GetApiCalls) != 1 {
			t.Fatalf("Expected API to be called but the api wasn't called")
		}

		want := GetApiCall{zoneID: zoneID, rr: cloudflare.DNSRecord{Type: "A"}}

		if spyApi.GetApiCalls[0] != want {
			t.Errorf("expected %v to equal %v", spyApi.GetApiCalls[0], want)
		}

		if !reflect.DeepEqual(result, spyApi.ReturnValue) {
			t.Errorf("Expected client to return %v, got %v", spyApi.ReturnValue, result)
		}
	})
	t.Run("client returns error if api fails", func(t *testing.T) {
		spyApi := &SpyApi{
			ReturnValue:    nil,
			ReturnGetError: errors.New("cloudflare API call failed"),
		}
		zoneID := "123"
		client := NewClient(spyApi, zoneID)

		_, err := client.GetARecords()

		if len(spyApi.GetApiCalls) != 1 {
			t.Fatalf("Expected API to be called but the api wasn't called")
		}

		if err == nil {
			t.Errorf("Expected resolver to return error but got none")
		}
	})
}

func TestUpdateDNSRecords(t *testing.T) {
	t.Run("update content of a dns record", func(t *testing.T) {
		spyApi := &SpyApi{}
		zoneID := "123"
		client := NewClient(spyApi, zoneID)
		oldRecord := cloudflare.DNSRecord{ID: "321", Content: "old"}
		newContent := "new"

		ctx := context.Background()

		err := client.UpdateRecordContent(ctx, oldRecord, newContent)

		if err != nil {
			t.Errorf("expected to receive no error but got %s", err.Error())
		}

		if len(spyApi.UpdateApiCalls) != 1 {
			t.Error("expected api update method to be called once but got called none")
		}

		gotContent := spyApi.UpdateApiCalls[0].NewRecord.Content
		if gotContent != newContent {
			t.Errorf("expected content of dns record to be updated from %q to %q", gotContent, newContent)
		}
	})
	t.Run("update content of a dns record cancel when context cancels", func(t *testing.T) {
		spyApi := &SpyApi{}
		zoneID := "123"
		client := NewClient(spyApi, zoneID)
		oldRecord := cloudflare.DNSRecord{ID: "321", Content: "old"}
		newContent := "new"

		ctx := context.Background()
		cancellingCtx, cancel := context.WithCancel(ctx)
		time.AfterFunc(5*time.Millisecond, cancel)

		err := client.UpdateRecordContent(cancellingCtx, oldRecord, newContent)

		if err != cancellingCtx.Err() {
			t.Errorf("the context got cancelled but wrong error got returned: got %v, want: %v", err, cancellingCtx.Err())
		}

	})
	t.Run("client returns an error if api fails", func(t *testing.T) {
		spyApi := &SpyApi{
			ReturnUpdateError: errors.New("api failed"),
		}
		zoneID := "123"
		client := NewClient(spyApi, zoneID)
		oldRecord := cloudflare.DNSRecord{ID: "321", Content: "old"}
		newContent := "new"

		ctx := context.Background()

		err := client.UpdateRecordContent(ctx, oldRecord, newContent)

		if len(spyApi.UpdateApiCalls) != 1 {
			t.Error("expected api update method to be called once but got called none")
		}

		if err == nil {
			t.Errorf("Expected resolver to return error but got none")
		}
	})
}
