package homelab_dns

import (
	"context"
	"github.com/cloudflare/cloudflare-go"
	"net"
	"net/http"
	"testing"
	"time"
)

type SpyApp struct {
	Calls       []string
	ReturnError error
}

func (s *SpyApp) CheckAndUpdateRecords(ctx context.Context, hostname string) error {
	s.Calls = append(s.Calls, hostname)
	return s.ReturnError
}

type SpyLookup struct {
	LookUps     []string
	ReturnValue []net.IP
	ReturnError error
}

func (s *SpyLookup) LookupIP(host string) ([]net.IP, error) {
	s.LookUps = append(s.LookUps, host)
	return s.ReturnValue, s.ReturnError
}

type SpyApi struct {
	GetApiCalls       []GetApiCall
	UpdateApiCalls    []UpdateApiCall
	ReturnValue       []cloudflare.DNSRecord
	ReturnGetError    error
	ReturnUpdateError error
}

type ConcurrentSpyApi struct {
	GetApiCalls       []GetApiCall
	UpdateApiCalls    chan UpdateApiCall
	ReturnValue       []cloudflare.DNSRecord
	ReturnGetError    error
	ReturnUpdateError error
}

type GetApiCall struct {
	zoneID string
	rr     cloudflare.DNSRecord
}

type UpdateApiCall struct {
	ZoneID    string
	RecordID  string
	NewRecord cloudflare.DNSRecord
}

func (s *SpyApi) DNSRecords(ctx context.Context, zoneID string, rr cloudflare.DNSRecord) ([]cloudflare.DNSRecord, error) {
	s.GetApiCalls = append(s.GetApiCalls, GetApiCall{zoneID, rr})
	return s.ReturnValue, s.ReturnGetError
}

func (s *SpyApi) UpdateDNSRecord(ctx context.Context, zoneID string, recordID string, rr cloudflare.DNSRecord) error {
	time.Sleep(10 * time.Millisecond)
	s.UpdateApiCalls = append(s.UpdateApiCalls, UpdateApiCall{
		ZoneID:    zoneID,
		RecordID:  recordID,
		NewRecord: rr,
	})
	return s.ReturnUpdateError
}

func (s *ConcurrentSpyApi) DNSRecords(ctx context.Context, zoneID string, rr cloudflare.DNSRecord) ([]cloudflare.DNSRecord, error) {
	s.GetApiCalls = append(s.GetApiCalls, GetApiCall{zoneID, rr})
	return s.ReturnValue, s.ReturnGetError
}

func (s *ConcurrentSpyApi) UpdateDNSRecord(ctx context.Context, zoneID string, recordID string, rr cloudflare.DNSRecord) error {
	time.Sleep(10 * time.Millisecond)
	s.UpdateApiCalls <- UpdateApiCall{
		ZoneID:    zoneID,
		RecordID:  recordID,
		NewRecord: rr,
	}
	return s.ReturnUpdateError
}

func AssertResponseBody(t *testing.T, got string, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func AssertStatus(t *testing.T, got int, want int) {
	t.Helper()
	if got != want {
		t.Errorf("got status %d want %d", got, want)
	}
}

func NewPutCheckAndUpdateRequest() *http.Request {
	req, _ := http.NewRequest(http.MethodPut, "/invoke", nil)
	return req
}
