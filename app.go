package homelab_dns

import (
	"context"
	"errors"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"net/http"
)

type AppServer struct {
	app          App
	referenceUrl string
	http.Handler
}

func (a *AppServer) checkAndUpdateHandler(writer http.ResponseWriter, request *http.Request) {
	err := a.app.CheckAndUpdateRecords(request.Context(), a.referenceUrl)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	} else {
		writer.WriteHeader(http.StatusNoContent)
	}
}

type App interface {
	CheckAndUpdateRecords(ctx context.Context, hostname string) error
}

type DefaultApp struct {
	resolver Resolver
	client   Client
}

// CheckAndUpdateRecords Check out the IP to which given hostname is pointing at the moment and update all A Records of the given zone according to this IP Address
func (c *DefaultApp) CheckAndUpdateRecords(ctx context.Context, hostname string) error {
	newIP, err := c.resolver.Resolve(hostname)

	if err != nil {
		return errors.New(fmt.Sprintf("IP lookup of %s failed: ", hostname) + err.Error())
	}

	records, err := c.client.GetARecords()

	if err != nil {
		return errors.New("failed to receive A DNS records from the cloudflare api: " + err.Error())
	}

	errorChannel := make(chan error, len(records))

	for _, record := range records {
		if record.Content != newIP {
			go func(r cloudflare.DNSRecord) {
				err := c.client.UpdateRecordContent(ctx, r, newIP)
				if err != nil {
					if err == ctx.Err() {
						errorChannel <- err
					}
					errorChannel <- errors.New(fmt.Sprintf("update of the record %v with the new content '%s' failed: ", r, newIP) + err.Error())
				} else {
					errorChannel <- nil
				}
			}(record)
		} else {
			errorChannel <- nil
		}
	}

	errorString := ""
	for i := 0; i < cap(errorChannel); i++ {
		err := <-errorChannel
		if err != nil {
			errorString += "'" + err.Error() + "'\n"
		}
	}

	if errorString != "" {
		return errors.New(fmt.Sprintf("failed to update records: %v", errorString))
	}
	return nil
}

func NewDefaultApp(resolver Resolver, client Client) *DefaultApp {
	return &DefaultApp{
		resolver: resolver,
		client:   client,
	}
}

func NewAppServer(app App, url string) *AppServer {
	a := new(AppServer)
	a.app = app

	router := http.NewServeMux()
	router.Handle("/invoke", http.HandlerFunc(a.checkAndUpdateHandler))

	a.Handler = router
	a.referenceUrl = url

	return a
}
