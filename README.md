# homelab-dns

[![Build Status](https://gitlab.com/dominikstraessle/homelab-dns/badges/master/pipeline.svg)](https://gitlab.com/dominikstraessle/homelab-dns/-/pipelines) [![Coverage Report](https://gitlab.com/dominikstraessle/homelab-dns/badges/master/coverage.svg)](https://gitlab.com/dominikstraessle/homelab-dns/-/pipelines) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/dominikstraessle/homelab-dns)](https://goreportcard.com/report/gitlab.com/dominikstraessle/homelab-dns) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

Automatically update Cloudflare DNS records according to a dynamic domain name from an ISP.

![Image of Gopher](img/gopher-small.png)

## Problem
This go program solves a very specific problem with my personal infrastructure.

My server is running at home and the home router doesn't have a static public IP. The ISP rotates this public IP from time to time.
I have a custom domain using the Cloudflare DNS. They have A Records pointing to the public IP of my home router. But every time the ISP rotates the public IP, the records become invalid, and I have to update them manually. There is also a dynamic domain name from the ISP which constantly points to the public IP, but a CNAME pointing to this dynamic domain name is not what I need (A records are required).

![Diagramm of the problem](img/homelab-old.svg)

## Solution
This program does the following to solve the problem from above:

1. Get the actual public IP through a lookup of the ISP's dynamic domain name.
2. Check the A Records in the Cloudflare DNS
3. Update the A Records if required

I set up the program to be invoked hourly via a cron job.

![Diagramm of the solution](img/homelab-new.png)
