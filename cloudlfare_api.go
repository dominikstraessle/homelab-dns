package homelab_dns

import (
	"context"
	"errors"
	"github.com/cloudflare/cloudflare-go"
)

type Client struct {
	api    CloudflareAPI
	zoneID string
}

// Get all A records of the given #Client.zoneID
// Default API should be cloudflare.DNSRecord
func (c *Client) GetARecords() ([]cloudflare.DNSRecord, error) {
	result, err := c.api.DNSRecords(nil, c.zoneID, cloudflare.DNSRecord{Type: "A"})
	if err != nil {
		return nil, errors.New("cloudflare api request failed: " + err.Error())
	}
	return result, nil
}

// Update a given DNSRecord (oldRecord) with a new content (newContent)
// Default API should be cloudflare.UpdateDNSRecord
func (c *Client) UpdateRecordContent(ctx context.Context, oldRecord cloudflare.DNSRecord, newContent string) error {
	resultError := make(chan error, 1)

	go func() {
		select {
		case <-ctx.Done():
			return
		default:
			oldRecord.Content = newContent
			err := c.api.UpdateDNSRecord(nil, c.zoneID, oldRecord.ID, oldRecord)
			if err != nil {
				resultError <- errors.New("cloudflare api request failed: " + err.Error())
			}
			resultError <- err
		}
	}()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case err := <-resultError:
		return err
	}
}

func NewClient(api CloudflareAPI, zoneID string) *Client {
	return &Client{
		api:    api,
		zoneID: zoneID,
	}
}

type CloudflareAPI interface {
	DNSRecords(ctx context.Context, zoneID string, rr cloudflare.DNSRecord) ([]cloudflare.DNSRecord, error)
	UpdateDNSRecord(ctx context.Context, zoneID string, recordID string, rr cloudflare.DNSRecord) error
}
