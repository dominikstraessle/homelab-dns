package homelab_dns_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	homelab_dns "homelab-dns"
	"net"
	"reflect"
	"sort"
	"strings"
	"testing"
	"time"
)

func ExampleDefaultApp_CheckAndUpdateRecords() {
	// Setup Spy to simulate cloudflare and lookup IP
	spyLookup := &homelab_dns.SpyLookup{
		ReturnValue: []net.IP{net.ParseIP("0.0.0.0")},
	}
	resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
	spyApi := &homelab_dns.ConcurrentSpyApi{
		ReturnValue: []cloudflare.DNSRecord{
			{Name: "example.com", Content: "1.1.1.1", ID: "1"},
			{Name: "*.example.com", Content: "1.1.1.1", ID: "2"},
			{Name: "test.example.com", Content: "1.1.1.1", ID: "3"},
		},
		UpdateApiCalls: make(chan homelab_dns.UpdateApiCall, 3),
	}
	defer close(spyApi.UpdateApiCalls) // close the channel

	zoneID := "123"
	referenceHostname := "test.example.com"
	client := *homelab_dns.NewClient(spyApi, zoneID)
	app := homelab_dns.NewDefaultApp(resolver, client)
	err := app.CheckAndUpdateRecords(context.Background(), referenceHostname)

	if err != nil {
		fmt.Println("failed to check and update records")
	}

	// Output:
}

func BenchmarkCheckAndUpdateRecords(b *testing.B) {
	for i := 0; i < b.N; i++ {
		referenceIP := net.ParseIP("0.0.0.0")
		zoneID := "123"
		referenceHostname := "test.example.com"

		spyLookup := &homelab_dns.SpyLookup{
			ReturnValue: []net.IP{referenceIP},
		}
		resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
		dnsRecords := []cloudflare.DNSRecord{
			{Name: "example.com", Content: "1.1.1.1", ID: "1"},
			{Name: "*.example.com", Content: "1.1.1.1", ID: "2"},
			{Name: "test.example.com", Content: "1.1.1.1", ID: "3"},
		}
		spyApi := &homelab_dns.ConcurrentSpyApi{
			ReturnValue:    dnsRecords,
			UpdateApiCalls: make(chan homelab_dns.UpdateApiCall, 3),
		}
		client := *homelab_dns.NewClient(spyApi, zoneID)
		app := homelab_dns.NewDefaultApp(resolver, client)

		err := app.CheckAndUpdateRecords(context.Background(), referenceHostname)

		if err != nil {
			b.Logf("no error expected but got: %v", err.Error())
		}

		close(spyApi.UpdateApiCalls) // close the channel
	}
}

func TestApp(t *testing.T) {
	t.Run("check and update records", func(t *testing.T) {
		referenceIP := net.ParseIP("0.0.0.0")
		zoneID := "123"
		referenceHostname := "test.example.com"

		spyLookup := &homelab_dns.SpyLookup{
			ReturnValue: []net.IP{referenceIP},
		}
		resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
		dnsRecords := []cloudflare.DNSRecord{
			{Name: "example.com", Content: "1.1.1.1", ID: "1"},
			{Name: "*.example.com", Content: "1.1.1.1", ID: "2"},
			{Name: "test.example.com", Content: referenceIP.String(), ID: "3"},
		}
		spyApi := &homelab_dns.ConcurrentSpyApi{
			ReturnValue:    dnsRecords,
			UpdateApiCalls: make(chan homelab_dns.UpdateApiCall, 2),
		}
		client := *homelab_dns.NewClient(spyApi, zoneID)
		app := homelab_dns.NewDefaultApp(resolver, client)

		err := app.CheckAndUpdateRecords(context.Background(), referenceHostname)

		var gotUpdates []homelab_dns.UpdateApiCall
		for i := 0; i < 2; i++ {
			result := <-spyApi.UpdateApiCalls
			gotUpdates = append(gotUpdates, result)
		}

		if err != nil {
			t.Errorf("should not return any error but returned %s", err.Error())
		}

		if len(spyLookup.LookUps) != 1 {
			t.Errorf("expected lookup function to be called once but got called %v times", len(spyLookup.LookUps))
		}

		if len(gotUpdates) != 2 {
			t.Errorf("cloudflare api should be called %v times but got called %v times", len(dnsRecords), len(gotUpdates))
		}

		var wantUpdateRecords []homelab_dns.UpdateApiCall

		for _, record := range dnsRecords {
			if record.Content != referenceIP.String() {
				record.Content = referenceIP.String()
				wantUpdateRecords = append(wantUpdateRecords, homelab_dns.UpdateApiCall{
					ZoneID:    zoneID,
					RecordID:  record.ID,
					NewRecord: record,
				})
			}
		}

		sort.Slice(gotUpdates, func(i, j int) bool {
			return gotUpdates[i].RecordID < gotUpdates[j].RecordID
		})

		if !reflect.DeepEqual(gotUpdates, wantUpdateRecords) {
			t.Errorf("expected to call cloudflare api with the following arguments '%v' but got '%v'", wantUpdateRecords, gotUpdates)
		}
	})
	t.Run("check and update records with cancelling context", func(t *testing.T) {
		referenceIP := net.ParseIP("0.0.0.0")
		zoneID := "123"
		referenceHostname := "test.example.com"

		spyLookup := &homelab_dns.SpyLookup{
			ReturnValue: []net.IP{referenceIP},
		}
		resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
		dnsRecords := []cloudflare.DNSRecord{
			{Name: "example.com", Content: "1.1.1.1", ID: "1"},
			{Name: "*.example.com", Content: "1.1.1.1", ID: "2"},
			{Name: "test.example.com", Content: referenceIP.String(), ID: "3"},
		}
		spyApi := &homelab_dns.ConcurrentSpyApi{
			ReturnValue:    dnsRecords,
			UpdateApiCalls: make(chan homelab_dns.UpdateApiCall, 2),
		}
		client := *homelab_dns.NewClient(spyApi, zoneID)
		app := homelab_dns.NewDefaultApp(resolver, client)
		ctx := context.Background()
		cancellingCtx, cancel := context.WithCancel(ctx)
		time.AfterFunc(5*time.Millisecond, cancel)

		err := app.CheckAndUpdateRecords(cancellingCtx, referenceHostname)

		if err == nil {
			t.Errorf("an error should be returned but got none")
		}

		if !strings.Contains(err.Error(), "context") {
			t.Errorf("error should include context but got %v", err.Error())
		}

		for i := 0; i < 2; i++ {
			<-spyApi.UpdateApiCalls
		}
	})
	t.Run("lookup returns an error", func(t *testing.T) {
		zoneID := "123"
		referenceHostname := "test.example.com"

		spyLookup := &homelab_dns.SpyLookup{
			ReturnError: errors.New("lookup error"),
		}
		resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
		spyApi := &homelab_dns.ConcurrentSpyApi{}
		client := *homelab_dns.NewClient(spyApi, zoneID)
		app := homelab_dns.NewDefaultApp(resolver, client)

		err := app.CheckAndUpdateRecords(context.Background(), referenceHostname)

		if err == nil {
			t.Errorf("expected to get an error but got none")
		}
	})

	t.Run("get records returns an error", func(t *testing.T) {
		zoneID := "123"
		referenceHostname := "test.example.com"
		referenceIP := net.ParseIP("0.0.0.0")

		spyLookup := &homelab_dns.SpyLookup{
			ReturnValue: []net.IP{referenceIP},
		}
		resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
		spyApi := &homelab_dns.ConcurrentSpyApi{
			ReturnGetError: errors.New("get records error"),
		}
		client := *homelab_dns.NewClient(spyApi, zoneID)
		app := homelab_dns.NewDefaultApp(resolver, client)

		err := app.CheckAndUpdateRecords(context.Background(), referenceHostname)

		if err == nil {
			t.Errorf("expected to get an error but got none")
		}
	})

	t.Run("update records returns an error", func(t *testing.T) {
		referenceIP := net.ParseIP("0.0.0.0")
		zoneID := "123"
		referenceHostname := "test.example.com"

		spyLookup := &homelab_dns.SpyLookup{
			ReturnValue: []net.IP{referenceIP},
		}
		resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
		dnsRecords := []cloudflare.DNSRecord{
			{Name: "example.com", Content: "1.1.1.1", ID: "1"},
			{Name: "*.example.com", Content: "1.1.1.1", ID: "2"},
			{Name: "test.example.com", Content: referenceIP.String(), ID: "3"},
		}
		spyApi := &homelab_dns.ConcurrentSpyApi{
			ReturnValue:       dnsRecords,
			ReturnUpdateError: errors.New("update failed"),
			UpdateApiCalls:    make(chan homelab_dns.UpdateApiCall, 2),
		}
		client := *homelab_dns.NewClient(spyApi, zoneID)
		app := homelab_dns.NewDefaultApp(resolver, client)

		err := app.CheckAndUpdateRecords(context.Background(), referenceHostname)

		if err == nil {
			t.Errorf("expected to get an error but got none")
		}

		for i := 0; i < 2; i++ {
			<-spyApi.UpdateApiCalls
		}
	})
}
