FROM golang:alpine AS build-env
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 CC=gcc go build -a -installsuffix cgo -o App -mod=mod cmd/webserver/main.go

FROM alpine:latest
WORKDIR /app
COPY --from=build-env /app/App /app/App
RUN chmod +x /app/App

EXPOSE 8080
ENTRYPOINT ["/app/App"]
